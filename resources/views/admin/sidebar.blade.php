<aside class="main-sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ url('img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{$user->name}}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Pesquisar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
        </div>
    </form>
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">NAVEGAÇÃO</li>
            @can('admin-index')
            <li {{ (Request::is('admin') ? 'class=active' : '') }}>
                <a href="{{URL::route('admin')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @endcan
            @can('admin-user-index')
                <li class="treeview
                    {{ (Request::is('admin/user') ? 'active' : '') }}
                    {{ (Request::is('admin/role') ? 'active' : '') }}
                    {{ (Request::is('admin/nanny') ? 'active' : '') }}
                " >
                    <a href="#">
                        <i class="fa fa-pencil"></i> <span>Cadastro</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li {{ (Request::is('admin/user') ? 'class=active' : '') }}>
                            <a href="{{URL::route('admin.user.index')}}">
                                <i class="fa fa-user"></i>
                                <span>Usuários</span>
                            </a>
                        </li>
                        <li {{ (Request::is('admin/role') ? 'class=active' : '') }}>
                            <a href="{{URL::route('admin.role.index')}}">
                                <i class="fa fa-users"></i>
                                <span>Papeis</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
        </ul>
    </section>
</aside>